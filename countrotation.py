# find number of rotations on sorted array
def countRotation(a):
	n = len(a)
	l = 0
	r = n-1

	while l <= r:
		mid = (l+r)//2
		# take modulus to maintain index within range
		nxt = (mid+1) % n 
		prev = (mid-1+n) % n # (a-b)%m
		
		if a[mid]<=a[prev] and a[mid]<=a[nxt]:
			return mid
		# move left
		if a[mid] <= a[r]:
			r = mid-1
		
		# right or a[mid] >= a[r]
		elif a[l]<=a[mid]:
			l = mid+1
		
if __name__ == '__main__':
	a = list(map(int, input().split()))
	print(countRotation(a))
