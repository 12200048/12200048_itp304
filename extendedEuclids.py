# extended euclids algorithm to find x, y
# ax + by = gcd(a,b)

def extendGCD(a, b):
	if b == 0:
		return [1, 0]

	result = extendGCD(b, a%b)

	x1 = result[1] # y
	y1 = result[0] - (a//b) * result[1] # x - (a/b)*y

	x = x1
	y = y1

	return [x,y]


if __name__ == '__main__':
	a = 12
	b = 30

	print(extendGCD(12, 30))