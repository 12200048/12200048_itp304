#  O(n+m)
def KMP(patr, m, s):
	n = len(s)
	pi = [0]*m

	i, j = 1, 0 # pi[0]=0, start i=1
	# build pi array
	while i < m:
		if patr[i] == patr[j]: # aa
			pi[i] = j+1
			i, j = i+1, j+1
		else:
			if j == 0:  # ab
				i += 1  # update only i
			else: # aab, update index j
				j = pi[j-1] # move to left

	i = 0 # string index
	j = 0 # pattern index

	while i < n:
		if s[i]==patr[j]: # s=abc, p=ac
			i, j = i+1, j+1 
		else:
			if j == 0: # s=ab, p = cd
				i += 1 # move to next char in s
			else:			# 
				j = pi[j-1]  # move j to left, update index j
		
		if j == m:
			print(i-m) # pattern found at this index
			# print(i-j) or
			j = pi[j-1] # update index j


if __name__ == '__main__':
	s = "ababcabcabababd"
	patr = "abc"
	KMP(s, patr)