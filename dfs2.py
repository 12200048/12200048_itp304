import collections 

class Graph:
	# constructor
	def __init__(self):
		self.graph = collections.defaultdict(list)
		
	def add_edge(self, src, dest):
		self.graph[src].append(dest)
		self.graph[dest].append(src)

	def dfs(self, src, visited=None):
		if visited is None:
			visited = set()

		visited.add(src)
		print(src, end=" ")
		for nbr in self.graph[src]:
			if nbr not in visited:
				self.dfs(nbr, visited)

		return 

if __name__ == '__main__':
	g = Graph() 
	g.add_edge(0, 1)
	g.add_edge(0, 2)
	g.add_edge(1, 3)
	g.add_edge(1, 4)
	g.add_edge(2, 5)
	g.add_edge(2, 6)
	# print(g.graph)
	g.dfs(0)
			