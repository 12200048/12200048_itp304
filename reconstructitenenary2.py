import collections

class Solution: 
	# functions returns new itinerary of type list
	def findItinary(self, tickets):
		adjList = collections.defaultdict(list)
		# add neighbours for each node
		for src, dest in tickets:
			adjList[src].append(dest)
		
		# sort dict values to get route in lexical order
		for l in adjList.values():
			l.sort()

		result = ["JFK"] # starting airport
		# recursive dfs to reconstruct itinary
		def dfs(src):
			# visited all airports
			if len(result) == len(tickets)+1:
				return True
			
			# no adjacent node for src, backtrack
			if src not in adjList:
				return False
			
			for i, v in enumerate(adjList[src]):
				adjList[src].pop(i) # remove item by index
				result.append(v) 
				
				if dfs(v):
					return True
				# backtrack
				adjList[src].insert(i, v) # add v back to adjList
				result.pop() # remove last element added, v 

			return False

		dfs("JFK")
		return result

if __name__ == '__main__':
	g = Solution()
	# tickets = [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
	tickets = [["JFK", "SFO"], ["JFK", "ATL"], ["SFO", "ATL"], ["ATL", "JFK"], ["ATL", "SFO"]]
	print(g.findItinary(tickets))
	
	
