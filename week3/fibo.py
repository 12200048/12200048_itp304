def fibo(n):
	if n == 0 or n==1:
		return n
	f1 = fibo(n-1)
	f2 = fibo(n-2)

	return f1+f2
if __name__ == '__main__':
	print(fibo(5))