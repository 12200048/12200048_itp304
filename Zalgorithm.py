# O(n^2) not efficient
# def zFunc(s):
# 	n = len(s)
# 	z = [0]*n
# 	for i in range(n):
# 		while i+z[i]<n and s[z[i]] == s[i+z[i]]:
# 			z[i] += 1
# 	return z		

# O(n)
def zFunc(s):
	n = len(s)
	z = [0]*n
	l = r = 0 # current rightmost segment match
	# i to traverse s 
	# l<=i<=r<n
	for i in range(1, n):
		if i<=r: # i inside rightmost segment match s[L,R] == s[0,R-L]
			# use previous computed values to assign new values
			z[i] = min(r-i+1, z[i-l]) # z[i-l] head start
		# i>r, compare values one by one
		else: # find new RMS match
			while i+z[i] < n and s[z[i]]==s[i+z[i]]:
				z[i]+=1
		# update rightmost segment range, new l and r
		if i+z[i]-1>r:
			l = i
			r = i+z[i]-1

	return z

if __name__ == '__main__':
	s = "abababcd"
	p = "ab"
	s = p+"$"+s
	z = zFunc(s)
	m = len(p)
	for i in range(m+1, len(z)):
		if z[i]==m:
			print(i-m-1, end=" ")