# O(logN) complexity

def freqcount(arr, item):
	s = 0
	e = len(arr)-1
	
	l = s
	r = e

	first = 0
	# lowerbound
	while l <= r:
		mid = (l+r)//2
		if arr[mid] == item:
			first = mid
			r = mid - 1
		# right
		elif item > arr[mid]:
			l = mid+1
		# left
		else:
			r = mid-1

	# print(l, r)
	last = 0
	# upper bound
	while s <= e:
		mid = (s+e)//2
		if arr[mid] == item:
			last = mid
			s = mid + 1
		# right
		elif item > arr[mid]:
			l = mid+1
		# left
		else:
			e = mid-1
	
	return last - first + 1

if __name__ == '__main__':
	arr = [1,2,2,3,3,3,3,4]
	k = 2
	print(freqcount(arr, k))